#!/usr/bin/python3.7
import camelot
from optparse import OptionParser
import os
import pickle
import PyPDF2
import re
import rkipdfdata

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  help="Analyze input file FILE", metavar="FILE")

parser.add_option("-o", "--outputfile", dest="output_filename",
                  help="Write ouput to csv file FILE", metavar="FILE")

parser.add_option("--debug", action="store_true", dest="debug",
                  help="Show debugging output")

parser.add_option("--plot", action="store_true", dest="plot",
                  help="Show debugging output")

parser.add_option("--nocache", action="store_true", dest="nocache",
                  help="Force re-evaluation of file without using cache")

parser.add_option("-a", "--actualRKI", action="store_true", dest="actualRKI", help="Get actual table from RKI")

(options, args) = parser.parse_args()

# Get actual RKI report and use this as input file for the script
if options.actualRKI:
    print("Getting latest RKI report from last wednesday")
    rkidata = rkipdfdata.RkiData()
    options.filename = rkidata.download_rki_pdf_from_last_wednesday()

if not options.filename:
    print("Please specify input file.")
    exit()

def dbg_print(arg):
    if options.debug:
        print(arg)
    else:
        pass

# Determine PDF page count. Used for improving robustness when processing PDF in English language.
num_pages = PyPDF2.PdfFileReader(open(options.filename, 'rb')).getNumPages()

# Build string of page numbers where to search for the table of interest
min_page = 8
max_page = 14
if max_page > num_pages:
    max_page = num_pages
pages_string = str(min_page)
for page_number in range(min_page+1, max_page+1):
    pages_string += "," + str(page_number)

# Read PDF file or retrieve parsed table data from cache file
cache_filename = os.path.join("cache/", os.path.basename(options.filename[:-4]) + ".p")
if os.path.exists(cache_filename) and not options.nocache:
    dbg_print("Loading data from cache file: " + cache_filename)
    tables = pickle.load(open(cache_filename, "rb"))
else:
    # Parse page range in which we expect to find the table.
    tables = camelot.read_pdf(options.filename, flavor='stream', pages=pages_string)
    pickle.dump(tables, open(cache_filename, "wb"))

# Returns True if this is the table with the test numbers, False otherwise
def has_test_numbers(table):
    for i, row in table.df.iterrows():
        for j, col in row.iteritems():
            if re.search("Positivenquote", col):
                return True
            if re.search("Positivenrate", col):
                return True
            if re.search("Proportion positive", col):
                return True
    return False

def get_table_language(table):
    for i, row in table.df.iterrows():
        for j, col in row.iteritems():
            if re.search("Positivenquote", col):
                return "DE"
            if re.search("Positivenrate", col):
                return "DE"
            if re.search("Proportion positive", col):
                return "EN"
    return "ERROR"


dbg_print("Number of identified tables: " + str(tables.n))
for table in tables:
    for i, row in table.df.iterrows():
        for j, col in row.iteritems():
            dbg_print("Item [" + str(i) + "][" + str(j) + "]: " + col)

def remove_thousands_sep(string):
    string = string.replace(",", "")
    string = string.replace(".", "")
    return string

def parse_table(table):
    language = get_table_language(table)
    if language == "EN":
        row_number = -1
        output_string = ""
        for i, row in table.df.iterrows():
            if re.match("Up until", row[0]):
                row_number = 0
            # Row #1 contains the numbers for the first line in EN PDF
            if row_number == 1:
                week = "Bis einschließlich KW10"
                total_tests = int(remove_thousands_sep(row[1]))
                positive_tests = int(remove_thousands_sep(row[2]))
                # Convert comma to dot before typecast
                positive_percent = float(row[3].replace(",", "."))
                num_labs = int(remove_thousands_sep(row[4]))
                output_string += str(week) + ","
                output_string += str(total_tests) + ","
                output_string += str(positive_tests) + ","
                output_string += str(positive_percent) + ","
                output_string += str(num_labs) + "\n"
            # Skip this row since it contains nothing we need
            elif row_number == 2:
                pass
            elif row_number > 2:
                if re.match("Total", row[0]):
                    break
                week = row[0].replace("week ", "")
                total_tests = int(remove_thousands_sep(row[1]))
                positive_tests = int(remove_thousands_sep(row[2]))
                # Convert comma to dot before typecast
                positive_percent = float(row[3].replace(",", "."))
                num_labs = int(remove_thousands_sep(row[4]))
                output_string += str(week) + ","
                output_string += str(total_tests) + ","
                output_string += str(positive_tests) + ","
                output_string += str(positive_percent) + ","
                output_string += str(num_labs) + "\n"

            if row_number > -1:
                row_number += 1
        return output_string
    elif language == "DE":
        row_number = -1
        output_string = ""
        for i, row in table.df.iterrows():
            if re.match("Bis einschließlich KW10", row[0]):
                row_number = 0
            if row_number > -1:
                if re.match("Summe", row[0]):
                    break
                week = row[0]
                total_tests = int(remove_thousands_sep(row[1]))
                positive_tests = int(remove_thousands_sep(row[2]))
                # Convert comma to dot before typecast
                positive_percent = float(row[3].replace(",", "."))
                num_labs = int(remove_thousands_sep(row[4]))
                output_string += str(week) + ","
                output_string += str(total_tests) + ","
                output_string += str(positive_tests) + ","
                output_string += str(positive_percent) + ","
                output_string += str(num_labs) + "\n"

            if row_number > -1:
                row_number += 1
        return output_string
    

for table in tables:
    if has_test_numbers(table):
        dbg_print("Table has test numbers.")
        for i, row in table.df.iterrows():
            for j, col in row.iteritems():
                dbg_print("Item [" + str(i) + "][" + str(j) + "]: " + col)

        output_string = "Kalenderwoche,Anzahl Testungen,Positiv getestet,Positivenquote (%),Anzahl übermittelnde Labore\n"
        output_string += parse_table(table)

        if options.output_filename:
            output_file = open(options.output_filename, "w")
            output_file.write(output_string)
            output_file.close()
        else:
            print(output_string)
       
    else:
        dbg_print("Table does not have test numbers.")






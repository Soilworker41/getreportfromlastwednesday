#!/usr/bin/python3.7
import camelot
from optparse import OptionParser
import re
import rkipdfdata
import datetime

parser = OptionParser()
parser.add_option("-f", "--file", dest="filename",
                  help="Analyze input file FILE", metavar="FILE")
parser.add_option("-c", "--confirmed", action="store_true", dest="confirmed",
                  help="Show number of confirmed cases")
parser.add_option("-d", "--deaths", action="store_true", dest="deaths",
                  help="Show number of death cases")
parser.add_option("-p", "--percentdeaths", action="store_true", dest="percent_deaths",
                  help="Show confirmed cases")
parser.add_option("-r", "--recovered", action="store_true", dest="recovered",
                  help="Show confirmed cases")

parser.add_option("--debug", action="store_true", dest="debug",
                  help="Show debugging output")

parser.add_option("--date", action="store_true", dest="date", help="Get RKI report by date")

(options, args) = parser.parse_args()

if options.date:
    rkidata = rkipdfdata.RkiData()
    if len(args) == 0:
        # Get RKI Report from Today
        today = datetime.date.today()
        options.filename = rkidata.download_rki_pdf_daily_report_bydate(today.year, today.month, today.day)
    else:
        if len(args) == 3:
            # Get RKI Report from cmd-line arguments args[0] = year, args[1]= month, args[2] = day
            options.filename = rkidata.download_rki_pdf_daily_report_bydate(int(args[0]), int(args[1]), int(args[2]))
        else:
            print("Wrong number of arguments for the date are given!")





if not options.filename:
    print("Please specify input file.")
    exit()


tables = camelot.read_pdf(options.filename, flavor='lattice', table_areas=['0,550,600,750'])

confirmed_strings = ["Bestätigte Fälle", "Confirmed cases"]
def contains_confirmed(item):
    for string in confirmed_strings:
        if re.match(string, str(item)):
            return True
    return False

percent_deaths_strings = ["Anteil Verstorbene", "Deaths \(%\)"]
def contains_percent_deaths(item):
    for string in percent_deaths_strings:
        if re.match(string, str(item)):
            return True
    return False

deaths_strings = ["Verstorbene", "Deaths"]
def contains_deaths(item):
    for string in deaths_strings:
        if re.match(string, str(item)) and not contains_percent_deaths(item):
            return True
    return False

recovered_strings = ["Genesene", "Recovered"]
def contains_recovered(item):
    for string in recovered_strings:
        if re.match(string, str(item)):
            return True
    return False

if options.debug:
    print("Total tables extracted:", tables.n)
    for table in tables:
        for i, row in table.df.iterrows():
            for j, col in row.iteritems():
                if contains_confirmed(col):
                    print("Item contains confirmed cases:")
                if contains_deaths(col):
                    print("Item contains death cases:")
                if contains_percent_deaths(col):
                    print("Item contains percent deaths:")
                print("Item [" + str(i) + "][" + str(j) + "]: " + col)

# Returns number of confirmed cases
def num_confirmed():
    for i, row in tables[0].df.iterrows():
        for j, col in row.iteritems():
            if contains_confirmed(col):
                cell_string = col
                cells = re.split('\n', cell_string)
                num_cases = cells[1]
                # Remove English or German thousands separator
                num_cases = num_cases.replace(".", "")
                num_cases = num_cases.replace(",", "")
                num_cases = int(num_cases)
                return num_cases
    return -1

# Returns number of deaths
def num_deaths():
    for i, row in tables[0].df.iterrows():
        for j, col in row.iteritems():
            if contains_deaths(col):
                cell_string = col
                cells = re.split('\n', cell_string)
                num_deaths = cells[1]
                # Remove English or German thousands separator
                num_deaths = num_deaths.replace(".", "")
                num_deaths = num_deaths.replace(",", "")
                num_deaths = int(num_deaths)
                return num_deaths
    return -1

def percent_deaths():
    for i, row in tables[0].df.iterrows():
        for j, col in row.iteritems():
            if contains_percent_deaths(col):
                # Anteil Verstorbene %
                cell_string = col
                cells = re.split('\n', cell_string)
                percent_deaths = cells[1]
                percent_deaths = percent_deaths.replace(",",".")
                percent_deaths = percent_deaths.replace("%","")
                percent_deaths = float(percent_deaths)
                return percent_deaths
    return -1

def num_recovered():
    for i, row in tables[0].df.iterrows():
        for j, col in row.iteritems():
            if contains_recovered(col):
                # Genesene
                cell_string = col
                cells = re.split('\n', cell_string)
                num_recovered = cells[1]
                num_recovered = num_recovered.replace("ca.", "")
                num_recovered = num_recovered.replace("*", "")
                # Remove English or German thousands separator
                num_recovered = num_recovered.replace(".", "")
                num_recovered = num_recovered.replace(",", "")
                num_recovered = int(num_recovered)
                return num_recovered
    return -1

if options.debug:
    print("Confirmed cases: " + str(num_confirmed()))
    print("Number of deaths: " + str(num_deaths()))
    print("Percent deaths: " + str(percent_deaths()))
    print("Number of recovered cases: " + str(num_recovered()))

if options.confirmed:
    print(num_confirmed())
if options.deaths:
    print(num_deaths())
if options.percent_deaths:
    print(percent_deaths())
if options.recovered:
    print(num_recovered())



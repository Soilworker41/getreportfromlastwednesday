# Download RKI Report from last Wednesday
import rkipdfdata
import camelot
import tkinter
import matplotlib
matplotlib.use('TkAgg')
# matplotlib.use('TkAgg')
# matplotlib.use('TKAgg', warn=False, force=True)

# main routine
if __name__ == '__main__':
    rkidata = rkipdfdata.RkiData()
    # Download any report from the archived reports (not the actual month reports!)
    # filename = rkidata.download_rki_pdf_daily_report_bydate(2020, 9, 16)
    # print(filename)

    # Download the daily report from last wednesday with the testing-table included
    filename = rkidata.download_rki_pdf_from_last_wednesday()
    print(filename)

    # Download the last available AGI report based on monthly reports

    # In case you need a previous report from couple of weeks you can
    # rkidata.set_date_for_agi_report(2020, 7, 16)

    # Get Report based on actual date if set_date is not called, otherwise it will return the report from the given
    # date or the week(s) before in case no report was available in this week of the given date
    # filename = rkidata.download_last_agi_pdf()
    # print(filename)

    # camelot test
    # tables = camelot.read_pdf("background_lines.pdf", flavor='stream', table_areas=['50,565,547,294'])
    # tables = camelot.read_pdf("background_lines.pdf", flavor='lattice')

    # camelot.plot(tables[0], kind='text').show()
    # print(tables[0].df)

import datetime
import requests
import re


agi_calendarweek_reports = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 24, 28, 32, 36, 39,
                            40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50]

rki_monthly_subfolders = {1: "Janu", 2: "Febr", 3: "Maer", 4: "Apri", 5: "Mai", 6: "Juni", 7: "Juli",
                          8: "Augu", 9: "Sept", 10: "Okt", 11: "Nov", 12: "Dez"}


class RkiData:
    """RKI Class to get PDFs from daily status reports of Covid-19\
       and the monthly report from AGI (Arbeitsgemeinschaft Influenza)"""

    def __init__(self):
        self.today = datetime.date.today()
        self.today_agi_report = datetime.date.today()
        self.now = datetime.datetime.now()

    def set_date_for_agi_report(self, year, month, day):
        self.today_agi_report = self.today.replace(year=year, month=month, day=day)

    def get_today(self):
        return self.today

    def download_last_agi_pdf(self):
        # Download location
        url = 'https://influenza.rki.de/Wochenberichte/2019_2020/2020-36.pdf'

        # Determine Calender week
        today = self.today_agi_report
        calenderweek = datetime.date(today.year, today.month, today.day).strftime("%V")

        # Search if actual calendarweek is available in List, if not subtract 1 and search again
        # until the week is found in the last
        calenderweek_int = int(calenderweek)
        while not agi_calendarweek_reports.count(calenderweek_int):
            calenderweek_int = calenderweek_int - 1

        last_report_week = calenderweek_int

        # Identify date in string of Rki PDF report
        reqexp_search_string = r'\d{4}-\d{2}'
        searched_url = re.sub(reqexp_search_string, str(self.today_agi_report.year) + "-" + str(last_report_week), url)

        # Download last available AGI File and save it into pdf-file
        file = requests.get(searched_url)
        filename = f'Agi_LastReport_{str(self.today_agi_report.year) + "-" + str(last_report_week)}.pdf'
        open(filename, 'wb').write(file.content)
        return filename

    def download_rki_pdf_from_last_wednesday(self):
        # Download location
        url = 'https://www.rki.de/DE/Content/InfAZ/N/' \
              'Neuartiges_Coronavirus/Situationsberichte/2020-09-01-de.pdf?__blob=publicationFile'

        # Function variables
        d = datetime
        wednesday_int = 2

        # Determine last Wednesday
        # Wednesday = 2 - int(current day) = Subtraction of current day to find Date of last Wednesday
        now = self.now
        current_time_hour = now.time().hour
        current_day = now.weekday()

        # Simulate next Monday / Tuesday for testing purposes
        # now = now + d.timedelta(days=2)
        # current_day = d.datetime.weekday(now)

        subtraction_value = wednesday_int - current_day
        if subtraction_value < 0:
            # For Thursday - Sunday
            last_wednesday = now + d.timedelta(days=subtraction_value)
        else:
            if subtraction_value == 0 and current_time_hour > 18:
                # For Wednesday 6 p.m. when the new report is available
                last_wednesday = now
            else:
                # For Monday and Tuesday
                last_wednesday = now + d.timedelta(days=(subtraction_value - 7))

        # Identify Subfolder for month
        found_month_key = rki_monthly_subfolders[now.month]
        year_string = now.year
        add_string = found_month_key + "_" + str(year_string) + "/"

        # Identify date in string of Rki PDF report
        reqexp_search_string = r'\d{4}-\d{2}-\d{2}'
        last_wednesday_date = last_wednesday.date()
        searched_url = re.sub(reqexp_search_string, add_string + str(last_wednesday_date), url)

        # Download RKI File from last Wednesday and save it into pdf-file
        file = requests.get(searched_url)
        filename = f'Rki_LastReport_TestCountTable_{last_wednesday.date()}.pdf'
        open(filename, 'wb').write(file.content)
        return filename

    def download_rki_pdf_daily_report_bydate(self, year, month, day):
        # Download location for archived daily reports (only working for older months than
        # actual the month
        url = 'https://www.rki.de/DE/Content/InfAZ/N/' \
              'Neuartiges_Coronavirus/Situationsberichte/2020-09-01-de.pdf?__blob=publicationFile'

        # Check if date lies in the future or before 4th of March 2020
        self.today = self.today.replace(year=year, month=month, day=day)

        if self.today > datetime.date.today():
            print("Date lies in the future, no report can be obtained!")
            return -1
        else:
            if year == 2020 and month <= 3 and day <= 3:
                print("No report available for dates before 4th of March 2020!")
                return -2

        # Identify date in string of Rki PDF report
        reqexp_search_string = r'\d{4}-\d{2}-\d{2}'
        report_date = self.today

        # Determine if current date is within current month then use subfolder in url
        # otherwise don't use subfolder in url
        now = self.now
        found_month_key = None
        if now.month != self.today.month:
            searched_url = re.sub(reqexp_search_string, str(report_date), url)
        else:
            found_month_key = rki_monthly_subfolders[now.month]
            year_string = now.year
            add_string = found_month_key + "_" + str(year_string) + "/"
            searched_url = re.sub(reqexp_search_string, add_string + str(report_date), url)
            print(searched_url)

        # Download RKI File from last Wednesday and save it into pdf-file
        file = requests.get(searched_url)
        filename = f'Rki_Report_Archived_{report_date}.pdf'
        open(filename, 'wb').write(file.content)
        return filename
